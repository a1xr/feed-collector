"use strict";

var _pipeUniverse = _interopRequireDefault(require("./pipeUniverse"));

var _a1xrFeedManager = require("a1xr-feed-manager");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var pipeUniverse =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    var exchangeCode, exchangeApi, uniPipe;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            exchangeCode = _a1xrFeedManager.ExchangeServices.ExchangeCodes.BINANCE_CODE;
            exchangeApi = _a1xrFeedManager.ExchangeServices.GetExchange(exchangeCode);
            uniPipe = new _pipeUniverse["default"](exchangeApi, _a1xrFeedManager.UniverseService, exchangeCode);
            return _context.abrupt("return", uniPipe.pipe());

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function pipeUniverse() {
    return _ref.apply(this, arguments);
  };
}();

var pipeAll =
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(credentials) {
    var universe;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return pipeUniverse();

          case 2:
            universe = _context2.sent;
            return _context2.abrupt("return", Promise.all([universe]));

          case 4:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function pipeAll(_x) {
    return _ref2.apply(this, arguments);
  };
}();

module.exports = {
  PipeAll: pipeAll,
  PipeUniverse: pipeUniverse
};