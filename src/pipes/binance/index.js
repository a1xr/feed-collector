import UniversePipe from './pipeUniverse';
import {ExchangeServices, UniverseService} from 'a1xr-feed-manager';

let pipeUniverse = async function(){

    var exchangeCode = ExchangeServices.ExchangeCodes.BINANCE_CODE;
    var exchangeApi = ExchangeServices.GetExchange(exchangeCode);

    var uniPipe = new UniversePipe(exchangeApi, UniverseService, exchangeCode);
    return uniPipe.pipe();
};

let pipeAll = async function(credentials){
    
    var universe = await pipeUniverse();

    return Promise.all([universe]);
};

module.exports = {
    PipeAll : pipeAll,
    PipeUniverse : pipeUniverse
};