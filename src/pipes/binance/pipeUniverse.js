
class PipeUniverse{
    
    constructor(binanceApi, universeService, exchangeCode){
        this.api = binanceApi;
        this.service = universeService;
        this.exchangeCode = exchangeCode;
    }

    async pipe(){
        var data = await this.api.getExchangeUniverse();
        //console.log(data);
        var reqs = [];
        for(var i = 0; i < data.length; i++){
            
            var req = this.service.updateEntityData({
                symbol : data[i].symbol,
                name : data[i].name,
                exchanges: [{
                    code : this.exchangeCode,
                    active : true
                }],
                alias : []
            });
            reqs.push(req);
        }
        return Promise.all(reqs);
    }
}


module.exports = PipeUniverse;

