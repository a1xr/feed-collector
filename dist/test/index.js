"use strict";

var _index = require("../index");

var expect = require('chai').expect;

describe('Binance Pipes', function () {
  describe('Universe', function () {
    it('status', function (done) {
      _index.BinancePipes.PipeUniverse().then(function (result) {
        expect(result.length > 0).to.equal(true);
        done();
      });
    });
  });
});