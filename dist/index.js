"use strict";

require("@babel/polyfill");

var _binance = _interopRequireDefault(require("./pipes/binance"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

module.exports = {
  BinancePipes: _binance["default"]
};